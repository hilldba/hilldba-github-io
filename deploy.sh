#!/usr/bin/env sh
NAME=$(echo "$1" | tr '[:upper:]' '[:lower:]' | sed 's/_/-/g')

cp wrangler.toml wrangler.toml.tmp
printf "\\n[env.$NAME]\\nname=\\"$NAME\\"\\n" >> wrangler.toml
echo $NAME

yarn generate
npx wrangler publish --env $NAME

mv wrangler.toml.tmp wrangler.toml
